// import external dependencies
import 'jquery';
import {Expo,TweenMax} from "gsap";

import 'smoothstate';




// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';







$(function() {


    $('.grid-item').click(function() {

          // Toggle the Body Class "show-nav"
          $(this).toggleClass('expanded');


          // Deactivate the default behavior of going to the next page on click 
   

    });
});







TweenMax.to(".star__logo", 1, {  width: "8rem", transform:"translateY(4vh)",delay:3,ease:Expo.easeIn});

TweenMax.to(".star__burst", .85, {  opacity:"1",delay:3.5,ease:Expo.easeIn});
TweenMax.to(".ground--back", 1, {  fill:"#272067",delay:3,ease:Expo.easeIn});
TweenMax.to(".ground--middle", 1, {  fill:"#1E1759",delay:3,ease:Expo.easeIn});
TweenMax.to(".home", .85, {  backgroundColor:"#1A144F",delay:3.5,ease:Expo.easeIn});



TweenMax.to(".banner__wordmark", .5, {  transform:"translateY(0vh)" ,opacity:"0",delay:3,ease:Expo.easeIn});
TweenMax.to(".stars", .75, {  opacity:"1",delay:3.5,ease:Expo.easeIn});
//TweenMax.to(".banner__ground", .75, {  opacity:"1",delay:4.25,ease:Expo.easeIn});
//TweenMax.to(".banner__background", .75, {  opacity:"1",delay:3.5,ease:Expo.easeIn});







/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

