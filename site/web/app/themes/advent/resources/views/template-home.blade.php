{{--
  Template Name: Homepage
--}}

@extends('layouts.app')
@section('content')




<div class="calendar">

<!--==============================
=            Calendar            =
===============================-->


<div data-date="2017-12-1" class="grid-item day-1"><h1>1</h1></div>
<div data-date="2017-12-2" class="grid-item day-2"><h1>2</h1></div>
<div data-date="2017-12-3" class="grid-item day-3"><h1>3</h1></div>
<div  class="grid-item  no-hover  day-1--photo"></div>
<div data-date="2017-12-4" class="grid-item day-4"><h1>4</h1></div>

<div data-date="2017-12-5" class="grid-item day-5"><h1>5</h1></div>
<div data-date="2017-12-6" class="grid-item day-6"><h1>6</h1></div>
<div data-date="2017-12-7" class="grid-item day-7"><h1>7</h1></div>
<div data-date="2017-12-8" class="grid-item day-8"><h1>8</h1></div>
<div data-date="2017-12-9" class="grid-item day-9"><h1>9</h1></div>
<div data-date="2017-12-10" class="grid-item day-10"><h1>10</h1></div>
<div  class="grid-item  no-hover day-10-worship"></div>
<div  class="grid-item  no-hover day-11-cta"></div>
<div data-date="2017-12-11" class="grid-item day-11"><h1>11</h1></div>
<div data-date="2017-12-12" class="grid-item day-12"><h1>12</h1></div>
<div data-date="2017-12-13" class="grid-item day-13"><h1>13</h1></div>
<div data-date="2017-12-14" class="grid-item day-14"><h1>14</h1></div>
<div data-date="2017-12-15" class="grid-item day-15"><h1>15</h1></div>
<div class="grid-item  no-hover day-16-photo"></div>
<div data-date="2017-12-16" class="grid-item day-16"><h1>16</h1></div>
<div data-date="2017-12-17" class="grid-item day-17"><h1>17</h1></div>
<div  class="grid-item  no-hover day-17-worship"></div>
<div data-date="2017-12-18" class="grid-item day-18"><h1>18</h1></div>
<div data-date="2017-12-19" class="grid-item day-19"><h1>19</h1></div>
<div data-date="2017-12-20" class="grid-item day-20"><h1>20</h1></div>
<div data-date="2017-12-21" class="grid-item day-21"><h1>21</h1></div>
<div  class="grid-item  no-hover day-21-photo"></div>
<div data-date="2017-12-22" class="grid-item day-22"><h1>22</h1></div>
<div data-date="2017-12-23" class="grid-item day-23"><h1>23</h1></div>
<div data-date="2017-12-24" class="grid-item day-24"><h1>24</h1></div>
<div  class="grid-item no-hover  day-24-photo"></div>

<!--====  End of Calendar  ====-->

</div>
@stop